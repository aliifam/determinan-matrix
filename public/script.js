function detMC() {
    var mat_arr = [];
    var ul = document.getElementsByClassName('input-masukan')
    for (let index = 0; index < ul.length; index++) {
        var temp = document.getElementsByClassName('input-masukan')[index].value
        mat_arr.push(parseFloat(temp));
    }

    var minora = mat_arr[0];
    var minorb = mat_arr[1];
    var minorc = mat_arr[2];

    var hasil_a = (mat_arr[4]*mat_arr[8]) - (mat_arr[5]*mat_arr[7])
    var hasil_b = (mat_arr[3]*mat_arr[8]) - (mat_arr[5]*mat_arr[6])
    var hasil_c = (mat_arr[3]*mat_arr[7]) - (mat_arr[4]*mat_arr[6])

    var final_hasil = (minora * hasil_a) - (minorb * hasil_b) + (minorc * hasil_c)

    if (isNaN(final_hasil)) final_hasil = "ERROR: isi semua field!";


    document.getElementById('hasil-hitung').innerHTML = "Expansi Kofaktor = " + final_hasil

    console.log(final_hasil)
}


function detOBE() {
    var mat_arr = [];
    var mov = 0
    for (let index = 0; index < 3; index++) {
        var temp_arr = []
        for (let index = 0; index < 3; index++) {
            var temp = document.getElementsByClassName('input-masukan')[mov].value;
            temp_arr.push(parseFloat(temp));
            mov++;
        }
        mat_arr.push(temp_arr);
    }

    /*
    if (mat_arr[3] != 0) {
        if (math[0] < 0) {
            var kpk = math.lcm(mat_arr[3], mat_arr[0])
            var kpk1 = kpk / mat_arr[3]
            var kpk2 = kpk /mat_arr[0]

            mat_arr[3] = mat_arr[3] * kpk1
            mat_arr[4] = mat_arr[4] * kpk1
            mat_arr[5] = mat_arr[5] * kpk1

            mat_arr[3] = mat_arr[0] * kpk2 + mat_arr[3]  
            mat_arr[4] = mat_arr[1] * kpk2 + mat_arr[4]  
            mat_arr[5] = mat_arr[2] * kpk2 + mat_arr[5]
        } else {
            var kpk = math.lcm(mat_arr[3], mat_arr[0])
            var kpk1 = kpk / mat_arr[3]
            var kpk2 = kpk /mat_arr[0]

            mat_arr[3] = mat_arr[3] * kpk1
            mat_arr[4] = mat_arr[4] * kpk1
            mat_arr[5] = mat_arr[5] * kpk1

            mat_arr[3] = mat_arr[0] * kpk2 - mat_arr[3]  
            mat_arr[4] = mat_arr[1] * kpk2 - mat_arr[4]  
            mat_arr[5] = mat_arr[2] * kpk2 - mat_arr[5] 
        }
         
    }

    if (mat_arr[6] != 0) {
        if (math[0] < 0) {
            var kpk = math.lcm(mat_arr[3], mat_arr[0])
            var kpk1 = kpk / mat_arr[3]
            var kpk2 = kpk /mat_arr[0]

            mat_arr[3] = mat_arr[3] * kpk1
            mat_arr[4] = mat_arr[4] * kpk1
            mat_arr[5] = mat_arr[5] * kpk1

            mat_arr[3] = mat_arr[0] * kpk2 + mat_arr[3]  
            mat_arr[4] = mat_arr[1] * kpk2 + mat_arr[4]  
            mat_arr[5] = mat_arr[2] * kpk2 + mat_arr[5]
        } else {
            var kpk = math.lcm(mat_arr[6], mat_arr[0])
            var kpk1 = kpk / mat_arr[6]
            var kpk2 = kpk /mat_arr[0]

            mat_arr[6] = mat_arr[6] * kpk1
            mat_arr[7] = mat_arr[7] * kpk1
            mat_arr[8] = mat_arr[8] * kpk1

            mat_arr[6] = mat_arr[0] * kpk2 - mat_arr[6]  
            mat_arr[7] = mat_arr[1] * kpk2 - mat_arr[7] 
            mat_arr[8] = mat_arr[2] * kpk2 - mat_arr[8] 
        }
        
    }

    if (mat_arr[7] != 0) {
        if (math[4] < 0) {
            var kpk = math.lcm(mat_arr[7], mat_arr[4])
            var kpk1 = kpk / mat_arr[7]
            var kpk2 = kpk / mat_arr[4]

            mat_arr[7] = mat_arr[7] * kpk1
            mat_arr[8] = mat_arr[8] * kpk1

            mat_arr[7] = mat_arr[4] * kpk2 + mat_arr[7]
            mat_arr[8] = mat_arr[5] * kpk2 + mat_arr[8]
        } else {
            var kpk = math.lcm(mat_arr[7], mat_arr[4])
            var kpk1 = kpk / mat_arr[7]
            var kpk2 = kpk / mat_arr[4]

            mat_arr[7] = mat_arr[7] * kpk1
            mat_arr[8] = mat_arr[8] * kpk1

            mat_arr[7] = mat_arr[4] * kpk2 - mat_arr[7]
            mat_arr[8] = mat_arr[5] * kpk2 - mat_arr[8]
        }
        
    }

    console.log(mat_arr[0] * mat_arr[4] * mat_arr[8])

    console.log(mat_arr[0], mat_arr[1], mat_arr[2])
    console.log(mat_arr[3], mat_arr[4], mat_arr[5])
    console.log(mat_arr[6], mat_arr[7], mat_arr[8])
    */

    console.log(mat_arr)

    var hasil = math.det(mat_arr)

    if (isNaN(hasil)) hasil = "ERROR: isi semua field!";

    document.getElementById('hasil-obe').innerHTML = "Determinan OBE = " + hasil

}